const filmsApi = "https://ajax.test-danit.com/api/swapi/films";
const listOfFilms = document.querySelector(".list-of-films");

function fetchData(url) {
    return fetch(url)
        .then(response => response.json())
}

fetch(filmsApi)
    .then(response => response.json())
    .then(films => {
        films.forEach(film => {
            const characters = film.characters;
            const characterPromises = characters.map(charUrl => fetchData(charUrl));

            Promise.all(characterPromises)
                .then(characterDataArray => {
                    const characterNames = characterDataArray.map(characterData => characterData.name);
                    listOfFilms.innerHTML += `
                        <ul>
                          <li class="episode"><b>Episode:</b> ${film.episodeId}</li>
                          <li class="title"><b>Film:</b> ${film.name}</li>
                          <li class="characters"><b>Characters:</b> ${characterNames.join(", ")}</li>
                          <li class="info"><b>About:</b> ${film.openingCrawl}</li>
                        </ul>
                    `;
                })
                .catch(error => {
                    console.error("Ошибка при выполнении запроса:", error.message);
                });
        });
    })
    .catch(error => {
        console.error("Ошибка при выполнении запроса:", error.message);
    });


